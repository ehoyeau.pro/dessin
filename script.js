var canvas = document.getElementById("app");
var ctx = canvas.getContext("2d");

var color = document.getElementById("color");
var size = document.getElementById("size");

let isDrawing = false;
let x = 0;
let y = 0;

// getDessins()

var tabDessin =[

]

const rect = app.getBoundingClientRect();

document.getElementById("clear").onclick=function clear(){

    ctx.clearRect(0,0,1000,500);
}

app.addEventListener('mousedown', e => {
    x = e.clientX - rect.left;
    y = e.clientY - rect.top;
    isDrawing = true;
});

app.addEventListener('mousemove', e => {
    if (isDrawing === true) {
        drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
        x = e.clientX - rect.left;
        y = e.clientY - rect.top;
    }
});

window.addEventListener('mouseup', e => {
    if (isDrawing === true) {
        drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
        x = 0;
        y = 0;
        isDrawing = false;
    }

    setDessin()

});

function setDessin(){
    $.ajax({
        url: 'https://api.draw.codecolliders.dev/paths/add',
        method: 'POST',
        data: {
            path: tabDessin,
            strokeColor: color.value,
            lineWidth: size.value,
        }
    })

tabDessin.length =0

}


// // chargement des nouveaux messages
//
// function getDessins() {
//     $.ajax({
//         url: 'https://api.draw.codecolliders.dev/paths',
//         method: 'POST',
//         data: {
//         }
//     }).done(function (data) {
//
//         // affichage des nouveaux dessins dans la page
//
//         for (let draw of data) {
//             console.log(draw);
//
//             if(draw.createdAt > '2021-03-24 15:55:30') {
//                 for (j = 0; j < draw.path.length; j++) {
//                     ctx.beginPath()
//                     ctx.lineTo(draw.path[j][0], draw.path[j][1])
//                     ctx.stroke();
//                 }
//             }
//         }
//     });
// }

function drawLine(context, x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.strokeStyle = color.value;
    ctx.lineWidth = size.value;
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    tabDessin.push([x1,y1])
    ctx.stroke();
    ctx.closePath();
}